﻿using UnityEngine;
using System.Collections;

public class N3KDoubleJump : BaseState
{
    public override void Construct()
    {
        base.Construct();
        motor.VerticalVelocity = motor.JumpForce;
        immuneTime = 0.1f;
        motor.AirExhausted = true;
    }

    public override void Destruct()
    {
        base.Destruct();
    }

    public override Vector3 ProcessMotion(Vector3 input)
    {
        MotorHelper.KillVector(ref input, motor.WallVector);
        MotorHelper.ApplySpeed(ref input, motor.Speed);
        MotorHelper.ApplyGravity(ref input, ref motor.VerticalVelocity, motor.Gravity, motor.TerminalVelocity);

        return input;
    }

    public override void PlayerTransition()
    {
        base.PlayerTransition();

        if (motor.VerticalVelocity < 0)
            motor.ChangeState("N3KFalling");
    }
}